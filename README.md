## 3. Newsletter Renderer

### Requirements

node -v v7.7.3

### install notes

```
    npm install
```

### demo

```
  npm start
```

### tests

```
  npm test
```

### developer notes

For sure this renderer can be get extra functionality. I didn't handle nested blocks, set up width/height, align, valign
and so on. I can do this as well but it's endless list. My aim was to show the example of my work.