function Renderer(size) {
    size = parseInt(size);
    if (size < 20) {
        throw new Error("Size is too small. It must be greater or equal than 20");
    }
    this.size = size;
}

Renderer.prototype.render = function(data) {
    if (typeof data.rows === "undefined") {
        throw new Error("Data must include 'lines' root attribute");
    }

    var html = '';

    for (var i in data.rows) {
        html += this.horizontalSeparator(html);
        html += this.renderRow(data.rows[i]);
    }
    html += this.horizontalSeparator(html);

    return html;
};

Renderer.prototype.horizontalSeparator = function() {
    var html = '';
    for (var i = 0; i < this.size; i++) {
        html += '-'
    }
    return html + "\n";
};


Renderer.prototype.renderRow = function(row) {
    if (typeof row.cols === "undefined") {
        throw new Error("Row must include 'cols' attribute");
    }

    var cols = row.cols;
    var widthWithoutSeparators = this.size - cols.length - 1;
    var width = Math.floor(widthWithoutSeparators / cols.length);

    var columns = [];
    for (var i = 0; i < cols.length; i++) {
        var isLast = i == cols.length - 1;
        if (isLast) {
            width = widthWithoutSeparators - ((cols.length - 1) * width);
        }
        columns.push(this.renderCol(cols[i], width));
    }

    var maxLines = 0;
    columns.forEach(function(column) {
        maxLines = Math.max(maxLines, column.length);
    });

    columns = columns.map(function(column) {
        if (column.length == maxLines) {
            return column;
        }
        var colWidth = column[0].length;
        for (var i = column.length; i < maxLines; i++) {
            column.push(fillStr(colWidth));
        }
        return column;
    });

    var html = '';
    for (var j = 0; j < maxLines; j++) {
        html += '|';
        for (var i = 0; i < columns.length; i++) {
            var column = columns[i];
            html += column[j] + "|";
        }
        html += "\n";
    }

    return html;
};

Renderer.prototype.renderCol = function(col, width) {
    if (width < 3) {
        throw new Error("Col width " + width + " is too small. Must be at least 3");
    }
    if (typeof col.type === "undefined") {
        throw new Error("Col must include 'type' attribute");
    }
    var colType = col.type;
    if (["text", "image"].indexOf(colType) === -1) {
        throw new Error("Undefined col type " + colType);
    }

    var lines = [];
    if (colType === "image") {
        lines.push(renderInCenterPos(width, "image"));
    } else if (colType === "text") {
        if (typeof col.value === 'undefined') {
            throw new Error("For text col value attribute must be specified");
        }

        var textValue = col.value;
        var linesCount = Math.ceil(textValue.length / (width - 2));
        var lineString;
        var pos = 0;
        for (var i = 0; i < linesCount; i++) {
            lineString = textValue.substr(pos, width - 2);
            lines.push(renderInCenterPos(width, lineString));
            pos += width - 2;
        }
    }
    return lines;
};

function renderInCenterPos(width, str) {
    if (width < str.length) {
        return str.substring(0, width);
    }

    var center = Math.floor((width - str.length) / 2);
    return fillStr(center) + str + fillStr(width - (center + str.length));
}

function fillStr(len, chr) {
    if (typeof chr === 'undefined') {
        chr = ' ';
    }
    return new Array(len + 1).join(chr);
}

module.exports = Renderer;