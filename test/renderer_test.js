var assert = require('assert');

var Renderer = require('../renderer');

describe('Renderer', function () {
    describe('RenderCol for image', function () {

        var renderer = new Renderer(50);

        it('renders one image col', function () {
            var lines = renderer.renderCol({"type": "image"}, 9);
            assert.equal('|' + lines.join() + '|', '|  image  |');
        });

        it('renders tiny image col', function () {
            var lines = renderer.renderCol({"type": "image"}, 5);
            assert.equal('|' + lines.join() + '|', '|image|');
        });

        it('renders cutted image col', function () {
            var lines = renderer.renderCol({"type": "image"}, 3);
            assert.equal('|' + lines.join() + '|', '|ima|');
        });

        it('renders cutted image col', function () {
            var lines = renderer.renderCol({"type": "image"}, 6);
            assert.equal('|' + lines.join() + '|', '|image |');
        });
    });

    describe('RenderCol for text', function () {

        var renderer = new Renderer(50);

        it('renders one text col', function () {
            var lines = renderer.renderCol({"type": "text", "value": "lorem"}, 10);
            assert.equal('|' + lines.join() + '|', "|  lorem   |");
        });

        it('throws an error if value attribute is not set', function () {
            var testCall = function () {
                renderer.renderCol({"type": "text"}, 10)
            };
            assert.throws(testCall, Error, "For text col value attribute must be specified");
        });

        it('renders one text col', function () {
            var lines = renderer.renderCol({"type": "text", "value": "lorem ipsum"}, 10);
            assert.equal('|' + lines.join("|\n|") + '|', "| lorem ip |\n|   sum    |");
        });
    });

    describe('RenderRow', function () {
        var renderer = new Renderer(20);

        it('renders one row with 2 columns', function () {
            var rowData = {
                "cols": [
                    {
                        "type": "image"
                    },
                    {
                        "type": "image"
                    }
                ]
            };
            var html = renderer.renderRow(rowData);
            assert.equal(html, "| image  |  image  |\n");
        });

        it('renders one row with 2 columns of 2 height', function () {
            var rowData = {
                "cols": [
                    {
                        "type": "text",
                        "value": "one line text",
                    },
                    {
                        "type": "text",
                        "value": "Lorem ipsum dolor sit amet, consectetur",
                    }
                ]
            };
            var renderer = new Renderer(50);
            var html = renderer.renderRow(rowData);
            var expectedHtml =
                "|     one line text     | Lorem ipsum dolor sit  |\n" +
                "|                       |   amet, consectetur    |\n";
            assert.equal(html, expectedHtml);
        });
    })

    describe('Renderer', function () {
        var renderer = new Renderer(20);

        it('renders row separator', function () {
            var html = renderer.horizontalSeparator();
            assert.equal(html, "--------------------\n");
        });


        var renderer = new Renderer(20);
        it('renders block', function () {
            var data = {
                "rows": [
                    {
                        "cols": [
                            {
                                "type": "image"
                            },
                            {
                                "type": "image"
                            }
                        ]
                    }]
            };
            var html = renderer.render(data);
            var expected =
                "--------------------\n" +
                "| image  |  image  |\n" +
                "--------------------\n";
            assert.equal(html, expected);
        });
    })
});