var fs = require('fs');
var Renderer = require('./renderer');

console.log("Render sample 1\n");
var renderer = new Renderer(23);
var data = JSON.parse(fs.readFileSync('./data/sample_1.json', 'utf8'));
var html = renderer.render(data);
console.log(html);
console.log("\n\n");


console.log("Render sample 2\n");
renderer = new Renderer(25);
data = JSON.parse(fs.readFileSync('./data/sample_2.json', 'utf8'));
html = renderer.render(data);
console.log(html);
console.log("\n\n");


console.log("Render sample 3\n");
renderer = new Renderer(25);
data = JSON.parse(fs.readFileSync('./data/sample_3.json', 'utf8'));
html = renderer.render(data);
console.log(html);
console.log("\n\n");